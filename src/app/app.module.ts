import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import { MaterialModule} from './material/material.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ListComponent } from './list/list.component';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FilterPipe } from './filter.pipe';
import { SingleComponent } from './single/single.component';

const appRoutes: Routes = [
  { path: 'show-contact/:id', component: SingleComponent },
  { path: 'contacts', component: ListComponent, data: { title: 'Contacts List' }},
  { path: '', redirectTo: 'contacts', pathMatch: 'full'}
];
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ListComponent,
    FilterPipe,
    SingleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
