import { Component, OnInit } from '@angular/core';
import {Router } from '@angular/router';
import {ContactsService} from '../contacts.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  contacts=[];
  constructor( private router: Router,private cService: ContactsService) { }

  ngOnInit() {
    this.contacts=this.cService.contacts;
  }
show(i){
  this.router.navigateByUrl('/show-contact/'+i)
}
}
