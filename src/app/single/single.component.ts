import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {ContactsService} from '../contacts.service';

@Component({
  selector: 'app-single',
  templateUrl: './single.component.html',
  styleUrls: ['./single.component.css']
})
export class SingleComponent implements OnInit {
index:number;
contacts=[];
  constructor(private route: ActivatedRoute,private cService: ContactsService) { }

  ngOnInit() {
    this.contacts=this.cService.contacts;
    this.index = this.route.snapshot.params.id;
    console.log(this.contacts)
  }

}
