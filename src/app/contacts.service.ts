import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})

export class ContactsService {
  contacts=[
    {userName: "Jhon Doe",phone: "0000000000"},
    {userName: "Jhane Doe",phone: "1111111111"},
    {userName: "Maugli Jingle",phone: "2222222222"},
    {userName: "Julia Roberts",phone: "3333333333"},
    {userName: "Katherine Keyta",phone: "4444444444"},
    {userName: "Lamjed Loukil",phone: "555555555"},
    {userName: "Jhon Travolta",phone: "666666666"}];
  constructor() { }
}
